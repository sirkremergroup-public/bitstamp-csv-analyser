## Info

This script will read an Bitstamp CSV and analyse transactions (deposits, withdrawals, buys, sells) to determine statistics (fees, average buy rate, average sell rate, return on investment) and current portfolio (available, tax free available, potential roi).

Known/potential issues:
- FIFO-function matching sells and buys have not yet been tested on partly sold buys
- Analysis only includes EUR, BTC and ETH transactions

## Install

Install python requests `pip3 install requests`.

Requests is needed to fetch current exchange rates from Bitstamp

## Usage

Download CSV from Bitstamp
Account > Transaction History > Export > Export All

CSV must be named `Transactions.csv` an placed in project directory

Cd into Projekt and run script using python3
`python3 convert.py`

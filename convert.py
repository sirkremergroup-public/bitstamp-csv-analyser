import csv
import datetime
try:
	import requests
except:
	print('ERROR: request package not installed (needed for fetching live rates)\n> pip3 install requests')
	quit()

BTC_VAL = float(requests.get(url='https://www.bitstamp.net/api/v2/ticker/btceur/').json()['last'])
ETH_VAL = float(requests.get(url='https://www.bitstamp.net/api/v2/ticker/etheur/').json()['last'])

def readFile(path='./Transactions.csv', delimiter=',', quotechar='"'):
	with open(path) as file:
		reader = csv.reader(file, delimiter=delimiter, quotechar=quotechar)
		columns = next(reader)
		data = [row for row in reader]

	return [{col: val for (col,val) in zip(columns,row)} for row in data]
def convertData(data):
	for obj in data:
		# parse currency
		obj['Currency'] = obj['Amount'][-3:]
		# parse date
		obj['Datetime'] = datetime.datetime.strptime(obj['Datetime'], '%b. %d, %Y, %I:%M %p')
		# parse amount
		obj['Amount'] = float(obj['Amount'][:-4])

		if obj['Currency'] != 'EUR':
			# parse value
			obj['Value'] = float(obj['Value'][:-4])
			# parse rate
			obj['Rate'] = float(obj['Rate'][:-4])
			# parse fee
			obj['Fee'] = float(obj['Fee'][:-4])
		elif obj['Type'] == 'Withdrawal':
			# parse fee
			obj['Fee'] = float(obj['Fee'][:-4])

def one_year_ago():
	now = datetime.datetime.now()
	try:
		then = now.replace(year=now.year-1)
	except ValueError:
		then = now.replace(year=now.year-1, day=now.day-1)
	return then

def add_cross_reference(buy, sell, amount):
	if not 'Refs' in buy:
		buy['Refs'] = []
	if not 'Refs' in sell:
		sell['Refs'] = []
	buy['Refs'].append({'Amount': amount, 'Ref': sell})
	sell['Refs'].append({'Amount': amount, 'Ref': buy})
def mark_as_sold(buy):
	buy['Sold'] = True
def match_fifo(bought, sold):

	i_buy, i_buy_max, i_sell, i_sell_max = 0, len(bought), 0, len(sold)
	# abort if none bought or sold
	if len(bought) == 0 or len(sold) == 0:
		return

	while True:
		# get amount bought
		it_amount_bought = bought[i_buy]['Amount']
		# subtract all matched sells
		if 'Refs' in bought[i_buy]:
			it_amount_bought -= sum(map(lambda d: d['Amount'], bought[i_buy]['Refs']))

		# get amount sold
		it_amount_sold = sold[i_sell]['Amount']
		# subtract all matched buys
		if 'Refs' in sold[i_sell]:
			it_amount_sold -= sum(map(lambda d: d['Amount'], sold[i_sell]['Refs']))

		if it_amount_bought <= 0.00000001:
			# skip to next
			mark_as_sold(bought[i_buy])
			i_buy += 1
		elif it_amount_sold <= 0.00000001:
			# skip to next
			i_sell += 1
		elif it_amount_sold == it_amount_bought:
			add_cross_reference(bought[i_buy], sold[i_sell], it_amount_sold)
			mark_as_sold(bought[i_buy])
			i_buy += 1
			i_sell += 1
		elif it_amount_sold > it_amount_bought:
			add_cross_reference(bought[i_buy], sold[i_sell], it_amount_bought)
			mark_as_sold(bought[i_buy])
			i_buy += 1
		else: # it_amount_sold < it_amount_bought
			add_cross_reference(bought[i_buy], sold[i_sell], it_amount_sold)
			i_sell += 1

		# exit condition (index overflow of i_buy should rightly throw error)
		if i_sell >= i_sell_max:
			break

	# split last, not completely sold buy
	if 'Refs' in bought[i_buy] and not 'Sold' in bought[i_buy]:
		amount_sold = sum(map(lambda d: d['Amount'], bought[i_buy]['Refs']))
		amount_bought = bought[i_buy]['Amount']

		bought[i_buy]['Amount'] = amount_sold
		bought[i_buy]['Fee'] *= amount_sold / amount_bought
		bought[i_buy]['Sold'] = True

		new_bought = bought[i_buy].copy()
		new_bought['Amount'] = amount_bought - amount_sold
		new_bought['Fee'] *= (amount_bought - amount_sold) / amount_bought
		del new_bought['Sold']
		del new_bought['Refs']
		bought.insert(i_buy+1, new_bought)

def calculateStats(data, btc_val=BTC_VAL, eth_val=ETH_VAL):

	# keys in data item:
	# std: Type, Datetime, Account, Amount, Value, Rate, Fee, Sub Type
	# own: Currency, Refs, Sold
	sublists = {}

	sublists['eur_deposits'] = [d for d in data if d['Type'] == 'Deposit' and d['Currency'] == 'EUR']
	sublists['eur_withdrawals'] = [d for d in data if d['Type'] == 'Withdrawal' and d['Currency'] == 'EUR']
	sublists['all_buys'] = [d for d in data if d['Sub Type'] == 'Buy']
	sublists['all_sells'] = [d for d in data if d['Sub Type'] == 'Sell']

	sublists['btc_buys'] = [d for d in data if d['Sub Type'] == 'Buy' and d['Currency'] == 'BTC']
	sublists['btc_sells'] = [d for d in data if d['Sub Type'] == 'Sell' and d['Currency'] == 'BTC']
	match_fifo(sublists['btc_buys'], sublists['btc_sells'])
	sublists['btc_buys_sold'] = [d for d in sublists['btc_buys'] if 'Sold' in d]
	sublists['btc_available'] = [d for d in sublists['btc_buys'] if not 'Sold' in d]
	sublists['btc_available_tax_free'] = [d for d in sublists['btc_available'] if d['Datetime'] < one_year_ago()]

	sublists['eth_buys'] = [d for d in data if d['Sub Type'] == 'Buy' and d['Currency'] == 'ETH']
	sublists['eth_sells'] = [d for d in data if d['Sub Type'] == 'Sell' and d['Currency'] == 'ETH']
	match_fifo(sublists['eth_buys'], sublists['eth_sells'])
	sublists['eth_buys_sold'] = [d for d in sublists['eth_buys'] if 'Sold' in d]
	sublists['eth_available'] = [d for d in sublists['eth_buys'] if not 'Sold' in d]
	sublists['eth_available_tax_free'] = [d for d in sublists['eth_available'] if d['Datetime'] < one_year_ago()]

	# for key in sublists:
	# 	print(f'{key:>20}\t{sublists[key]}')

	# money transfers
	eur_deposited = sum(map(lambda d: d['Amount'], sublists['eur_deposits']))
	eur_withdrawn = sum(map(lambda d: d['Amount'], sublists['eur_withdrawals']))
	eur_withdrawal_fees = sum(map(lambda d: d['Fee'], sublists['eur_withdrawals']))
	# spend on cryptos
	buy_fees = sum(map(lambda d: d['Fee'], sublists['all_buys']))
	eur_spend = sum(map(lambda d: d['Value'] + d['Fee'], sublists['all_buys']))
	# earned on cryptos
	sell_fees = sum(map(lambda d: d['Fee'], sublists['all_sells']))
	eur_converted = sum(map(lambda d: d['Value'] - d['Fee'], sublists['all_sells']))
	eur_converted_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['btc_buys_sold'] + sublists['eth_buys_sold']))
	# total transaction costs
	fees = eur_withdrawal_fees + buy_fees + sell_fees
	# earned on cryptos
	eur_available = eur_deposited - eur_spend + eur_converted - eur_withdrawn
	eur_invested = sum(map(lambda d: d['Value'] + d['Fee'], sublists['btc_available'] + sublists['eth_available']))
	eur_realised = eur_converted - eur_converted_cost

	# btc all bought
	btc_bought = sum(map(lambda d: d['Amount'], sublists['btc_buys']))
	btc_bought_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['btc_buys']))
	btc_bought_rate = btc_bought_cost / btc_bought if btc_bought != 0 else 0
	# btc all sold
	btc_sold = sum(map(lambda d: d['Amount'], sublists['btc_sells']))
	btc_sold_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['btc_buys_sold']))
	btc_sold_buy_rate = btc_sold_cost / btc_sold if btc_sold != 0 else 0
	btc_sold_return = sum(map(lambda d: d['Value'] - d['Fee'], sublists['btc_sells']))
	btc_sold_rate = btc_sold_return / btc_sold if btc_sold != 0 else 0
	btc_sold_roi = btc_sold_return / btc_sold_cost - 1 if btc_sold_cost != 0 else 0
	# btc all not sold
	btc_available = sum(map(lambda d: d['Amount'], sublists['btc_available']))
	btc_available_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['btc_available']))
	btc_available_buy_rate = btc_available_cost / btc_available if btc_available != 0 else 0
	btc_available_value = btc_available * btc_val
	btc_available_roi = btc_available_value / btc_available_cost - 1 if btc_available_cost != 0 else 0

	btc_available_tax_free = sum(map(lambda d: d['Amount'], sublists['btc_available_tax_free']))
	btc_available_tax_free_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['btc_available_tax_free']))
	btc_available_tax_free_buy_rate = btc_available_tax_free_cost / btc_available_tax_free if btc_available_tax_free != 0 else 0
	btc_available_tax_free_value = btc_available_tax_free * btc_val
	btc_available_tax_free_roi = btc_available_tax_free_value / btc_available_tax_free_cost - 1 if btc_available_tax_free_cost != 0 else 0

	# eth all bought
	eth_bought = sum(map(lambda d: d['Amount'], sublists['eth_buys']))
	eth_bought_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['eth_buys']))
	eth_bought_rate = eth_bought_cost / eth_bought if eth_bought != 0 else 0
	# eth all sold
	eth_sold = sum(map(lambda d: d['Amount'], sublists['eth_sells']))
	eth_sold_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['eth_buys_sold']))
	eth_sold_buy_rate = eth_sold_cost / eth_sold if eth_sold != 0 else 0
	eth_sold_return = sum(map(lambda d: d['Value'] - d['Fee'], sublists['eth_sells']))
	eth_sold_rate = eth_sold_return / eth_sold if eth_sold != 0 else 0
	eth_sold_roi = eth_sold_return / eth_sold_cost - 1 if eth_sold_cost != 0 else 0
	# eth all not sold
	eth_available = sum(map(lambda d: d['Amount'], sublists['eth_available']))
	eth_available_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['eth_available']))
	eth_available_buy_rate = eth_available_cost / eth_available if eth_available != 0 else 0
	eth_available_value = eth_available * eth_val
	eth_available_roi = eth_available_value / eth_available_cost - 1 if eth_available_cost != 0 else 0

	eth_available_tax_free = sum(map(lambda d: d['Amount'], sublists['eth_available_tax_free']))
	eth_available_tax_free_cost = sum(map(lambda d: d['Value'] + d['Fee'], sublists['eth_available_tax_free']))
	eth_available_tax_free_buy_rate = eth_available_tax_free_cost / eth_available_tax_free if eth_available_tax_free != 0 else 0
	eth_available_tax_free_value = eth_available_tax_free * eth_val
	eth_available_tax_free_roi = eth_available_tax_free_value / eth_available_tax_free_cost - 1 if eth_available_tax_free_cost != 0 else 0

	all_available_cost = btc_available_cost + eth_available_cost
	all_available_value = btc_available_value + eth_available_value
	all_available_roi = all_available_value / all_available_cost - 1 if all_available_cost != 0 else 0

	all_available_tax_free_cost = btc_available_tax_free_cost + eth_available_tax_free_cost
	all_available_tax_free_value = btc_available_tax_free_value + eth_available_tax_free_value
	all_available_tax_free_roi = all_available_tax_free_value / all_available_tax_free_cost - 1 if all_available_tax_free_cost != 0 else 0


	return {
	'sublists': sublists,
	'eur_deposited': eur_deposited,
	'eur_withdrawn': eur_withdrawn,
	'eur_withdrawal_fees': eur_withdrawal_fees,
	'buy_fees': buy_fees,
	'eur_spend': eur_spend,
	'sell_fees': sell_fees,
	'eur_converted': eur_converted,
	'eur_invested': eur_invested,
	'eur_realised': eur_realised,
	'fees': fees,
	'eur_available': eur_available,
	'btc_bought': btc_bought,
	'btc_bought_cost': btc_bought_cost,
	'btc_bought_rate': btc_bought_rate,
	'btc_sold': btc_sold,
	'btc_sold_cost': btc_sold_cost,
	'btc_sold_buy_rate': btc_sold_buy_rate,
	'btc_sold_return': btc_sold_return,
	'btc_sold_rate': btc_sold_rate,
	'btc_sold_roi': btc_sold_roi,
	'btc_available': btc_available,
	'btc_available_cost': btc_available_cost,
	'btc_available_buy_rate': btc_available_buy_rate,
	'btc_available_value': btc_available_value,
	'btc_available_roi': btc_available_roi,
	'btc_available_tax_free': btc_available_tax_free,
	'btc_available_tax_free_cost': btc_available_tax_free_cost,
	'btc_available_tax_free_buy_rate': btc_available_tax_free_buy_rate,
	'btc_available_tax_free_value': btc_available_tax_free_value,
	'btc_available_tax_free_roi': btc_available_tax_free_roi,
	'eth_bought': eth_bought,
	'eth_bought_cost': eth_bought_cost,
	'eth_bought_rate': eth_bought_rate,
	'eth_sold': eth_sold,
	'eth_sold_cost': eth_sold_cost,
	'eth_sold_buy_rate': eth_sold_buy_rate,
	'eth_sold_return': eth_sold_return,
	'eth_sold_rate': eth_sold_rate,
	'eth_sold_roi': eth_sold_roi,
	'eth_available': eth_available,
	'eth_available_cost': eth_available_cost,
	'eth_available_buy_rate': eth_available_buy_rate,
	'eth_available_value': eth_available_value,
	'eth_available_roi': eth_available_roi,
	'eth_available_tax_free': eth_available_tax_free,
	'eth_available_tax_free_cost': eth_available_tax_free_cost,
	'eth_available_tax_free_buy_rate': eth_available_tax_free_buy_rate,
	'eth_available_tax_free_value': eth_available_tax_free_value,
	'eth_available_tax_free_roi': eth_available_tax_free_roi,
	'all_available_cost': all_available_cost,
	'all_available_value': all_available_value,
	'all_available_roi': all_available_roi,
	'all_available_tax_free_cost': all_available_tax_free_cost,
	'all_available_tax_free_value': all_available_tax_free_value,
	'all_available_tax_free_roi': all_available_tax_free_roi
	}

def print_overview(stats):
	print(f'{"EUR":>23}\t{stats["eur_available"]:>7.2f}€ (In: {stats["eur_deposited"]:.2f}€; Active Investment: {stats["eur_invested"]:.2f}€; Realised Gains: {stats["eur_realised"]:.2f}€; Out: {stats["eur_withdrawn"]:.2f}€; Total Fees: {stats["fees"]:.2f}€)')
	print(f'{"invested EUR":>23}\t{stats["all_available_cost"]:>7.2f}€ (Value: {stats["all_available_value"]:>9.2f}€; ROI: {stats["all_available_roi"]*100:+.2f}%)')
	print(f'{"tax free invested EUR":>23}\t{stats["all_available_tax_free_cost"]:>7.2f}€ (Value: {stats["all_available_tax_free_value"]:>9.2f}€; ROI: {stats["all_available_tax_free_roi"]*100:+.2f}%)')
	print('')
	print(f'{BTC_VAL:>22.2f}€')
	print(f'{"available BTC":>23}\t{stats["btc_available"]:11.8f} at {stats["btc_available_buy_rate"]:>9.2f}€ for {stats["btc_available_cost"]:>7.2f}€ (Value: {stats["btc_available_value"]:.2f}€; ROI: {stats["btc_available_roi"]*100:+.2f}%)')
	print(f'{"tax free available BTC":>23}\t{stats["btc_available_tax_free"]:11.8f} at {stats["btc_available_tax_free_buy_rate"]:>9.2f}€ for {stats["btc_available_tax_free_cost"]:>7.2f}€ (Value: {stats["btc_available_tax_free_value"]:.2f}€; ROI: {stats["btc_available_tax_free_roi"]*100:+.2f}%)')
	print(f'{"total bought BTC":>23}\t{stats["btc_bought"]:11.8f} at {stats["btc_bought_rate"]:>9.2f}€ for {stats["btc_bought_cost"]:>7.2f}€')
	print(f'{"total sold BTC":>23}\t{stats["btc_sold"]:11.8f} at {stats["btc_sold_rate"]:>9.2f}€ for {stats["btc_sold_return"]:>7.2f}€ (Bought at {stats["btc_sold_buy_rate"]:.2f}€ for {stats["btc_sold_cost"]:.2f}€; ROI: {stats["btc_sold_roi"]*100:+.2f}%)')
	print('')
	print(f'{ETH_VAL:>22.2f}€')
	print(f'{"available ETH":>23}\t{stats["eth_available"]:11.8f} at {stats["eth_available_buy_rate"]:>9.2f}€ for {stats["eth_available_cost"]:>7.2f}€ (Value: {stats["eth_available_value"]:.2f}€; ROI: {stats["eth_available_roi"]*100:+.2f}%)')
	print(f'{"tax free available ETH":>23}\t{stats["eth_available_tax_free"]:11.8f} at {stats["eth_available_tax_free_buy_rate"]:>9.2f}€ for {stats["eth_available_tax_free_cost"]:>7.2f}€ (Value: {stats["eth_available_tax_free_value"]:.2f}€; ROI: {stats["eth_available_tax_free_roi"]*100:+.2f}%)')
	print(f'{"total bought ETH":>23}\t{stats["eth_bought"]:11.8f} at {stats["eth_bought_rate"]:>9.2f}€ for {stats["eth_bought_cost"]:>7.2f}€')
	print(f'{"total sold ETH":>23}\t{stats["eth_sold"]:11.8f} at {stats["eth_sold_rate"]:>9.2f}€ for {stats["eth_sold_return"]:>7.2f}€ (Bought at {stats["eth_sold_buy_rate"]:.2f}€ for {stats["eth_sold_cost"]:.2f}€; ROI: {stats["eth_sold_roi"]*100:+.2f}%)')

def print_sells(stats):
	print('Sells')
	for d in stats['sublists']['btc_sells']:
		print(f'{d["Datetime"]} - {d["Amount"]}')
		for ref in d['Refs']:
			print(f'>>> {ref["Ref"]["Datetime"]} - {ref["Amount"]} of {ref["Ref"]["Amount"]}')
def print_history_buys(stats):
	print('Buys')
	for d in stats['sublists']['btc_buys_sold']:
		print(f'{d["Datetime"]} - {d["Amount"]}')
		for ref in d['Refs']:
			print(f'>>> {ref["Ref"]["Datetime"]} - {ref["Amount"]} of {ref["Ref"]["Amount"]}')


if __name__ == '__main__':
	data = readFile()
	convertData(data)
	stats = calculateStats(data)
	print('')
	print_overview(stats)
	print('')
